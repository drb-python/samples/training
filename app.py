from flask import render_template, Flask
import _version
from datetime import datetime

app = Flask(__name__)


@app.route("/")
def home():
    Creation_Date = get_release_date()
    return render_template('summary.html', Creation_Date=Creation_Date)


@app.route("/drb_node.html")
def drb_node():
    return render_template('drb_node.html')


@app.route("/logical_node.html")
def logical_node():
    return render_template('logical_node.html')


@app.route("/drivers.html")
def drivers():
    return render_template('drivers.html')


@app.route("/topics.html")
def topics():
    return render_template('topics.html')


@app.route("/xquery.html")
def xquery():
    return render_template('xquery.html')


@app.route("/pycharm_training.html")
def pycharm_training():
    return render_template('pycharm_training.html')


@app.route("/about")
def about_description():
    cdate = get_release_date()

    about_description = {
        "version": _version.get_versions()['version'],
        "creation_date": cdate,
        "author": "Gael Systems",
        "sub-components": "",
        "description": "Drb is a library dedicated to simplify the access" +
        "to the data with a unique and as simple as possible interface."
    }
    return about_description


def get_release_date():
    service_release_date = _version.get_versions()['date']
    if len(str(service_release_date)) > 0:
        isodate = service_release_date
        pos = service_release_date.find('+')
        if pos > -1:
            isodate = service_release_date[:pos]

        date_time_obj = datetime.fromisoformat(isodate)
        cdate = date_time_obj.strftime("%Y-%m-%d")
    return cdate


if __name__ == '__main__':
    app.run()
