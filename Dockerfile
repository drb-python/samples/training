FROM python:3.8

COPY . .

RUN ["python", "-m", "pip", "install", "-r", "requirements.txt"]

EXPOSE 5000
ENTRYPOINT ["python"]
CMD ["-m", "flask", "run", "--host=0.0.0.0"]


